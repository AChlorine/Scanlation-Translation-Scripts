'''
DONE

FIND NUMBER OF CHAPTERS
FIND NUMBER OF PAGES PER CHAPTER

TO DO

CREATE FOLDERS BY CHAPTERS
FIND THE RIGHT PAGES FOR THE CHAPTERS
ASSIGN PAGES TO CHAPTERS
'''

import os
from script_format_helper_directory import create_format

path = "F:\\Rain"
number_of_chapters = []
pages_in_chapters = []

def get_numbers():
    print("get_numbers()")
    numbers = []
    beginning = 0
    end = 0
    
    try:
        beginning = int(input("Beginning: "))
        end = int(input("End: "))
        print("\n")
    except ValueError as exception:
        beginning = 0
        end = 0
        print("EXCEPTION CAUGHT: ", exception)
    finally:
        numbers.append(beginning)
        numbers.append(end)
    return numbers

def get_page_numbers(number_of_chapters):
    print("get_page_numbers()")
    pages_in_chapters = []
    for i in range(number_of_chapters[0], number_of_chapters[1]+1):
        print("Chapter ", i)
        pages_in_chapters.append(get_numbers())
    return pages_in_chapters

#PRINTS 
def find_files(pages_in_chapters):
    print("find_files()")
    for items in pages_in_chapters:
        beginning = items[0]
        end = items[1]
        for item in range(beginning, end+1):
            if(item < 10):
                print("File name: ", "000" + str(item) + ".png")
            else:
                print("File name: ", "00" + str(item) + ".png")
def main():    
    print("RAWs ORGANIZER")
    number_of_chapters = get_numbers()
    pages_in_chapters = get_page_numbers(number_of_chapters)
    find_files(pages_in_chapters)

if __name__ == "__main__":
    main()

