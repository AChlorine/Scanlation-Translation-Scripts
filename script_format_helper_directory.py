import os

path = "F:\\Alice or Alice\\"

def create_format(path, iskoma):
    files = os.listdir(path)
    number_of_files = len(files)
    for i in range(0,number_of_files):
        inside_path = path + files[i] + "\\"
        inside_files = os.listdir(inside_path)
        file_name = inside_path + files[i] + " TR.txt"
        number_of_files_inside = len(inside_files)

        tl_script_text = open(file_name, "w+")
        for j in range(0, number_of_files_inside):
            string = "-- " + inside_files[j] + " (Page " + str(j + 1) + ") --\n\n\n"
            tl_script_text.write(string) 
            if(iskoma == True):
                right_format = "Right: \n\tPanel 1:\n\tPanel 2:\n\tPanel 3:\n\tPanel 4:\n\n"
                left_format = "Left: \n\tPanel 1:\n\tPanel 2:\n\tPanel 3:\n\tPanel 4:\n\n\n"
                tl_script_text.write(right_format)
                tl_script_text.write(left_format)
            
        tl_script_text.close()

    print("Process completed successfully")
    
def main():
    create_format(path, True)
if __name__== "__main__":
    main()